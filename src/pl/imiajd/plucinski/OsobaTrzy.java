package pl.imiajd.plucinski;

import java.time.LocalDate;
import java.util.Objects;

public class OsobaTrzy implements Cloneable, Comparable<OsobaTrzy> {
    public OsobaTrzy(String nazwisko, LocalDate dataUrodzenia) {
        this.nazwisko = nazwisko;
        this.dataUrodzenia = dataUrodzenia;
    }
    public String getNazwisko() { return this.nazwisko; }
    public LocalDate getDataUrodzenia() { return this.dataUrodzenia; }

    @Override
    public String toString() {
        LocalDate du = this.getDataUrodzenia();
        return String.format("%s [%s, %04d-%02d-%02d]", this.getClass().getSimpleName(), this.getNazwisko(), du.getYear(), du.getMonthValue(), du.getDayOfMonth());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        OsobaTrzy osoba = (OsobaTrzy) o;
        return this.getNazwisko() == osoba.getNazwisko() &&
                this.getDataUrodzenia() == osoba.getDataUrodzenia();
    }

    public int compareTo(OsobaTrzy o) {
        int result = this.getNazwisko().compareTo(o.getNazwisko());
        return result != 0 ? result : this.getDataUrodzenia().compareTo(o.getDataUrodzenia());
    }

    @Override
    public int hashCode() {
        return Objects.hash(nazwisko, dataUrodzenia);
    }

    private String nazwisko;
    private LocalDate dataUrodzenia;
}