package pl.imiajd.plucinski;

import java.time.LocalDate;

public abstract class OsobaDwa
{
    public OsobaDwa(String[] imiona, String nazwisko, LocalDate dataUrodzenia, boolean plec)
    {
        this.imiona = imiona;
        this.nazwisko = nazwisko;
        this.dataUrodzenia = dataUrodzenia;
        this.plec = plec;
    }

    public abstract String getOpis();

    public String[] getImiona() { return this.imiona; }
    public LocalDate getDataUrodzenia() { return this.dataUrodzenia; }
    public String getNazwisko() { return this.nazwisko; }
    public boolean getPlec() { return this.plec; }

    private String[] imiona;
    private String nazwisko;
    private LocalDate dataUrodzenia;
    private boolean plec;
}
