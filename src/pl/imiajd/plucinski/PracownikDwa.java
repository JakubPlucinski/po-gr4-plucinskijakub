package pl.imiajd.plucinski;

import java.time.LocalDate;

public class PracownikDwa extends OsobaDwa
{
    public PracownikDwa(String[] imiona, String nazwisko, LocalDate dataUrodzenia, boolean plec, LocalDate dataZatrudnienia, double pobory)
    {
        super(imiona, nazwisko, dataUrodzenia, plec);
        this.dataZatrudnienia = dataZatrudnienia;
        this.pobory = pobory;
    }

    public LocalDate getDataZatrudnienia() { return this.dataZatrudnienia; }
    public double getPobory() { return this.pobory; }
    public String getOpis() {
        StringBuffer im = new StringBuffer("");
        for (String i : super.getImiona())
            im.append(String.format("%s ", i));

        return String.format("%s%s, ur. %04d-%02d-%02d\nZatrudniony %04d-%02d-%02d\npracownik z pensją %.2f zł", im.toString(), super.getNazwisko(), super.getDataUrodzenia().getYear(), super.getDataUrodzenia().getMonthValue(), super.getDataUrodzenia().getDayOfMonth(), this.getDataZatrudnienia().getYear(), this.getDataZatrudnienia().getMonthValue(), this.getDataZatrudnienia().getDayOfMonth(), pobory);
    }

    private LocalDate dataZatrudnienia;
    private double pobory;
}
