package pl.imiajd.plucinski;

public class Adres {
    public Adres(String miasto, String ulica, int numer_domu, int numer_mieszkania, String kod_pocztowy)
    {
        this.miasto = miasto;
        this.ulica = ulica;
        this.numer_domu = numer_domu;
        this.numer_mieszkania = numer_mieszkania;
        this.kod_pocztowy = kod_pocztowy;
    }

    public Adres(String miasto, String ulica, int numer_domu, String kod_pocztowy)
    {
        this.miasto = miasto;
        this.ulica = ulica;
        this.numer_domu = numer_domu;
        this.numer_mieszkania = 0;
        this.kod_pocztowy = kod_pocztowy;
    }

    public void pokaz()
    {
        System.out.println(String.format("%s %s\n%s %d%s", this.kod_pocztowy, this.miasto, this.ulica, this.numer_domu, this.numer_mieszkania == 0 ? "" : String.format("/%d", this.numer_mieszkania)));
    }

    public boolean przed(Adres zip)
    {
        return Integer.parseInt(this.kod_pocztowy) < Integer.parseInt(zip.kod_pocztowy);
    }

    private String miasto;
    private String ulica;
    private int numer_domu;
    private int numer_mieszkania;
    private String kod_pocztowy;
}
