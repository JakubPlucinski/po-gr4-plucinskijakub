package pl.imiajd.plucinski;

import java.time.LocalDate;

public class StudentDwa extends OsobaDwa {
    public StudentDwa(String[] imiona, String nazwisko, LocalDate dataUrodzenia, boolean plec, double sredniaOcen, String kierunek) {
        super(imiona, nazwisko, dataUrodzenia, plec);
        this.sredniaOcen = sredniaOcen;
        this.kierunek = kierunek;
    }

    public double getSredniaOcen() {
        return this.sredniaOcen;
    }

    public void setSredniaOcen(double nowaWartosc) {
        this.sredniaOcen = nowaWartosc;
    }

    public String getOpis() {
        StringBuffer im = new StringBuffer("");
        for (String i : super.getImiona())
            im.append(String.format("%s ", i));

        return String.format("%s%s, ur. %04d-%02d-%02d\nKierunek studiów: %s\nSrednia ocen: %.2f", im.toString(), super.getNazwisko(), super.getDataUrodzenia().getYear(), super.getDataUrodzenia().getMonthValue(), super.getDataUrodzenia().getDayOfMonth(), kierunek, this.getSredniaOcen());
    }

    private double sredniaOcen;
    private String kierunek;
}

