package pl.uwm.wmii.plucinskijakub.laboratorium05;

import pl.imiajd.plucinski.Nauczyciel;
import pl.imiajd.plucinski.Student;

public class Zadanie4 {
    public static void main(String[] args)
    {
        Student s = new Student("Sewruk", 1997, "informatyka");
        Nauczyciel n = new Nauczyciel("Kowalski", 1980,  5000);
        System.out.println(s.toString());
        System.out.println(n.toString());
    }
}
