package pl.uwm.wmii.plucinskijakub.laboratorium01;

import java.util.Scanner;

public class Zadanie22 {
    public static void main(String[] args)
    {
        Scanner in = new Scanner(System.in);

        System.out.print("Podaj ilość liczb: ");
        int x = in.nextInt();
        double[] tab;
        tab = new double[x];
        int suma=0;
        System.out.print("Podawaj liczby: ");
        for(int i = 0; i <x; i++)
        {
            tab[i] = in.nextDouble();
        }
        for(int i = 0; i<x;i++)
        {
            if(tab[i] > 0) suma+=tab[i];
        }
        System.out.print("Podwojna suma to: " +2*suma);
    }
}
