package pl.uwm.wmii.plucinskijakub.laboratorium01;

import java.util.Scanner;

public class Zadanie21d {
    public static void main(String[] args)
    {
        Scanner in = new Scanner(System.in);

        System.out.print("Podaj ilość liczb: ");
        int x = in.nextInt(), licznik=0;
        System.out.print("Podawaj liczby: ");
        int[] tab;
        tab = new int[x];
        for(int i = 0; i <x; i++)
        {
            tab[i] = in.nextInt();
        }
        for(int i = 1; i < x-1; i++)
        {
            if(tab[i] < tab[i-1]+tab[i+1]/2) licznik++;
        }

        System.out.println("ilość liczb = "+licznik);
    }
}
