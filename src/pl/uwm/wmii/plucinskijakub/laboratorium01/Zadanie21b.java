package pl.uwm.wmii.plucinskijakub.laboratorium01;

import java.util.Scanner;

public class Zadanie21b {
    public static void main(String[] args)
    {
        Scanner in = new Scanner(System.in);

        System.out.print("Podaj ilość liczb: ");
        int x = in.nextInt(), licznik=0, liczba;
        System.out.print("Podawaj liczby: ");
        for(int i = 0; i < x; i++)
        {
            liczba = in.nextInt();
            if(liczba%3==0 || liczba%5==1) licznik++;
        }

        System.out.println("ilość liczb = "+licznik);

    }
}
