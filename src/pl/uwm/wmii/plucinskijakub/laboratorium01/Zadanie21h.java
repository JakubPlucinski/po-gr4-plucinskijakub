package pl.uwm.wmii.plucinskijakub.laboratorium01;

import java.util.Scanner;

public class Zadanie21h {
    public static void main(String[] args)
    {
        Scanner in = new Scanner(System.in);

        System.out.print("Podaj ilość liczb: ");
        int x = in.nextInt(), licznik=0;
        System.out.print("Podawaj liczby: ");
        int[] tab;
        tab = new int[x];
        for(int i = 0; i <x; i++)
        {
            tab[i] = in.nextInt();
        }
        for(int i = 0; i < x; i++)
        {
            if(tab[i]< 0) tab[i]*=-1;
            if(tab[i]<i*i) licznik++;
        }

        System.out.println("ilość liczb = "+licznik);
    }
}
