package pl.uwm.wmii.plucinskijakub.laboratorium01;

import java.util.Scanner;

public class Zadanie23 {
    public static void main(String[] args)
    {
        Scanner in = new Scanner(System.in);

        System.out.print("Podaj ilość liczb: ");
        int x = in.nextInt();
        double[] tab;
        tab = new double[x];
        int dodatnie=0, ujemne=0, zera =0;
        System.out.print("Podawaj liczby: ");
        for(int i = 0; i <x; i++)
        {
            tab[i] = in.nextDouble();
        }
        for(int i = 0; i<x;i++)
        {
            if(tab[i] > 0) dodatnie+=1;
            if(tab[i] < 0) ujemne+=1;
            if(tab[i] == 0) zera+=1;
        }
        System.out.println("Ilość liczb dodatnich: " + dodatnie);
        System.out.println("Ilość liczb ujemnych: " + ujemne);
        System.out.println("Ilość zer: " + zera);
    }
}
