package pl.uwm.wmii.plucinskijakub.laboratorium01;

import java.util.Scanner;

public class Zadanie21f {
    public static void main(String[] args)
    {
        Scanner in = new Scanner(System.in);

        System.out.print("Podaj ilość liczb: ");
        int x = in.nextInt(), licznik=0;
        System.out.print("Podawaj liczby: ");
        int[] tab;
        tab = new int[x+1];
        tab[0] = 0;
        for(int i = 1; i <x+1; i++)
        {
            tab[i] = in.nextInt();
        }
        for(int i = 1; i < x+1; i++)
        {
            if(i%2==1 && tab[i]%2==0) licznik++;
        }

        System.out.println("ilość liczb = "+licznik);
    }
}
