package pl.uwm.wmii.plucinskijakub.laboratorium01;

import java.util.Scanner;

public class Zadanie21e {
    public static void main(String[] args)
    {
        Scanner in = new Scanner(System.in);

        System.out.print("Podaj ilość liczb: ");
        int x = in.nextInt(), licznik=0, silnia=1;
        System.out.print("Podawaj liczby: ");
        int[] tab;
        tab = new int[x];

        for(int i = 0; i <x; i++) {
            tab[i] = in.nextInt();
        }

        for(int i = 1; i < x; i++)
        {
            silnia*=i;
            if(tab[i] > Math.pow(2,i) && tab[i] <silnia ) licznik++;
        }

        System.out.println("ilość liczb = "+licznik);
    }
}
