package pl.uwm.wmii.plucinskijakub.laboratorium01;

import java.util.Scanner;

public class Zadanie24 {
    public static void main(String[] args)
    {
        Scanner in = new Scanner(System.in);

        System.out.print("Podaj ilość liczb: ");
        int x = in.nextInt();
        double[] tab;
        tab = new double[x];
        System.out.print("Podawaj liczby: ");
        for(int i = 0; i <x; i++)
        {
            tab[i] = in.nextDouble();
        }
        double najmniejsza = tab[0], najwieksza = tab[0];
        for(int i = 1; i<x;i++)
        {
            if(najmniejsza<tab[i]) najwieksza = tab[i];
            if(najwieksza>tab[i]) najmniejsza = tab[i];
        }
        System.out.println("Liczba najmniejsza: " + najmniejsza);
        System.out.println("Liczba najwieksza: " + najwieksza);

    }
}
