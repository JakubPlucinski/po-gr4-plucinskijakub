package pl.uwm.wmii.plucinskijakub.laboratorium01;

import java.util.Scanner;

public class Zadanie1e {
    public static void main(String[] args)
    {
        Scanner in = new Scanner(System.in);

        System.out.print("Podaj ilość liczb: ");
        double x = in.nextInt();
        double suma=0, liczba;
        System.out.print("Podawaj liczby: ");
        for(int i = 0; i < x; i++)
        {
            liczba = in.nextDouble();
            liczba=Math.pow(liczba, 2);
            suma += liczba;
        }

        System.out.println("wynik = "+suma);

    }
}
