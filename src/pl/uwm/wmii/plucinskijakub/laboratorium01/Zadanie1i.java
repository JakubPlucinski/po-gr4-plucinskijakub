package pl.uwm.wmii.plucinskijakub.laboratorium01;

import java.util.Scanner;

public class Zadanie1i {
    public static void main(String[] args)
    {
        Scanner in = new Scanner(System.in);

        System.out.print("Podaj ilość liczb: ");
        int x = in.nextInt();
        double suma=0, liczba, silnia=1;
        System.out.print("Podawaj liczby: ");
        for(int i = 1; i <=x; i++)
        {
            liczba = in.nextDouble();
            silnia*=i;
            if(i%2==0) suma += liczba/silnia;
            else suma -= liczba/silnia;
        }

        System.out.println("wynik = "+suma);

    }
}
