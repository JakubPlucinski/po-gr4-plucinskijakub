package pl.uwm.wmii.plucinskijakub.Laboratorium07;

import java.time.LocalDate;
import java.time.Period;
import java.time.temporal.ChronoUnit;

public class Osoba implements Comparable {
    public int ileLat() { return Period.between(this.dataUrodzenia, LocalDate.now()).getYears(); }
    public int ileMiesiecy() {
        return this.dataUrodzenia.withYear(LocalDate.now().getYear()).compareTo(LocalDate.now()) < 0 ?
                LocalDate.now().getMonthValue() - this.dataUrodzenia.getMonthValue() :
                12 - this.dataUrodzenia.getMonthValue() + LocalDate.now().getMonthValue();
    }
    public int ileDni() {
        return Math.abs((int)ChronoUnit.DAYS.between(LocalDate.now(), this.dataUrodzenia.withYear(LocalDate.now().getYear()).withMonth(LocalDate.now().getMonthValue())));
    }
    public Osoba(String nazwisko, LocalDate dataUrodzenia) {
        this.nazwisko = nazwisko;
        this.dataUrodzenia = dataUrodzenia;
    }
    @Override
    public String toString() {
        return String.format("%s [%s, %d-%02d-%02d]", this.getClass().getSimpleName(), this.nazwisko, this.dataUrodzenia.getYear(), this.dataUrodzenia.getMonthValue(), this.dataUrodzenia.getDayOfMonth());
    }
    @Override
    public boolean equals(Object otherObject) {
        if (this == otherObject)
            return true;
        if (otherObject == null)
            return false;
        if (!(otherObject instanceof Osoba))
            return false;
        Osoba other = (Osoba) otherObject;

        return this.nazwisko == other.nazwisko && this.dataUrodzenia == other.dataUrodzenia;
    }
    @Override
    public int compareTo(Object otherObject) {
        Osoba other = (Osoba) otherObject;
        int nazwiskoCompare = this.nazwisko.compareTo(other.nazwisko);
        int dataUrodzeniaCompare = this.dataUrodzenia.compareTo(other.dataUrodzenia);
        return nazwiskoCompare != 0 ? nazwiskoCompare : dataUrodzeniaCompare;
    }
    private String nazwisko;
    private LocalDate dataUrodzenia;
}
