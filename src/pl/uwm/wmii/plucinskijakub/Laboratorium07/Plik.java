package pl.uwm.wmii.plucinskijakub.Laboratorium07;

import java.io.*;
import java.util.*;

public class Plik {
    public static void main(String[] args) throws Exception {
        ArrayList<String> ar = new ArrayList<>();
        BufferedReader in = new BufferedReader(new FileReader("D:\\po-gr4-plucinskijakub\\src\\pl\\edu\\uwm\\wmii\\holubowiczbartosz\\lista10\\tekst.txt"));
        String s;
        while ((s = in.readLine()) != null)
            ar.add(s);
        ar.sort(new Comparator<String>() {
            public int compare(String a, String b) {
                return a.compareTo(b);
            }
        });
        System.out.println(ar.toString());
        in.close();
    }
}
