package pl.uwm.wmii.plucinskijakub.laboratorium07;

import java.time.LocalDate;
import java.util.Arrays;

public class TestOsoba {
    public static void main(String[] args) {
        Osoba[] grupa = new Osoba[5];
        grupa[0] = new Osoba("Sewruk", LocalDate.of(1995, 5, 20));
        grupa[1] = new Osoba("Sewruk", LocalDate.of(2000, 4, 15));
        grupa[2] = new Osoba("Kowalski", LocalDate.of(2001, 5, 15));
        grupa[3] = new Osoba("Nowak", LocalDate.of(2001, 5, 15));
        grupa[4] = new Osoba("Plucinski", LocalDate.of(1997, 9, 18));
        System.out.println(Arrays.toString(grupa));
        Arrays.sort(grupa);
        System.out.println(Arrays.toString(grupa));
        for (Osoba os : grupa)
            System.out.println(String.format("Lat: %d, Miesięcy: %d, Dni: %d", os.ileLat(), os.ileMiesiecy(), os.ileDni()));
    }
}
