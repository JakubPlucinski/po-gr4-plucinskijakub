package pl.uwm.wmii.plucinskijakub.laboratorium08;

public class PairDemo {
    public static void main(String[] args) {
        Pair<String> p = new Pair<>("Jakub", "Pluciński");
        p.swap();
        System.out.println(String.format("%s %s", p.getFirst(), p.getSecond()));
    }
}
