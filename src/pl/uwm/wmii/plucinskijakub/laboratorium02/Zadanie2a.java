package pl.uwm.wmii.plucinskijakub.laboratorium02;

import java.util.Random;
import java.util.Scanner;

public class Zadanie2a {
    public static void main(String[] args)
    {
        Scanner in = new Scanner(System.in);

        System.out.print("Podaj ilość liczb: ");
        int n = in.nextInt();
        int[] tab;
        tab = new int[n];
        generuj(tab, n, 999, 1998);
        int ileN = ileNieparzystych(tab);
        int ileP = ileParzystych(tab);
        System.out.println("");
        System.out.println("Ilość liczb nieparzystych: " + ileN);
        System.out.println("Ilość liczb parzystych: " + ileP);
    }
    public static void generuj(int tab[], int n, int minWartosc, int maxWartosc)
    {
        Random r = new Random();
        for(int j = 0; j < n; j++)
        {
            tab[j] = r.nextInt(maxWartosc) - minWartosc;
            System.out.print(tab[j] + " ");
        }
    }
    public static int ileNieparzystych(int[] tab)
    {
        int nieparzyste = 0;
        for(int i = 0; i <tab.length; i++)
        {
            if(tab[i]%2 !=0) nieparzyste++;
        }
        return nieparzyste;
    }
    public static int ileParzystych(int[] tab)
    {
        int parzyste = 0;
        for(int i = 0; i <tab.length; i++)
        {
            if(tab[i]%2 ==0) parzyste++;
        }
        return parzyste;
    }
}