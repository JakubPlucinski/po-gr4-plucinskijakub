package pl.uwm.wmii.plucinskijakub.laboratorium02;

import java.util.Scanner;
import java.util.Random;

public class Zadanie1a {
    public static void main(String[] args)
    {
        Scanner in = new Scanner(System.in);

        System.out.print("Podaj ilość liczb: ");
        int n = in.nextInt();
        int[] tab;
        tab = new int[n];
        int parzyste=0, nieparzyste =0;
        Random r =new Random();
        for(int i = 0; i <n; i++)
        {
            tab[i] = r.nextInt(1998) -999;
            System.out.print(tab[i] + " ");
            if(tab[i]%2 !=0) nieparzyste++;
            if(tab[i]%2 ==0) parzyste++;
        }
        System.out.println("");
        System.out.println("Ilość liczb nieparzystych: " + nieparzyste);
        System.out.println("Ilość liczb parzystych: " + parzyste);
    }
}
