package pl.uwm.wmii.plucinskijakub.laboratorium02;

import java.util.Random;
import java.util.Scanner;

public class Zadanie2f {
    public static void main(String[] args)
    {
        Scanner in = new Scanner(System.in);

        System.out.print("Podaj ilość liczb: ");
        int n = in.nextInt();
        int[] tab;
        tab = new int[n];
        generuj(tab, n, 999, 1998);
        System.out.println("");
        signum(tab);
    }
    public static void generuj(int tab[], int n, int minWartosc, int maxWartosc)
    {
        Random r = new Random();
        for(int j = 0; j < n; j++)
        {
            tab[j] = r.nextInt(maxWartosc) - minWartosc;
            System.out.print(tab[j] + " ");
        }
    }
    public static void signum(int[] tab)
    {
        for(int i = 0; i <tab.length; i++)
        {
            if(tab[i] < 0) tab[i] = -1;
            if(tab[i] > 0) tab[i] = 1;
            System.out.print(tab[i] + " ");
        }
    }
}
