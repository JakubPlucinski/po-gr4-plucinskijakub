package pl.uwm.wmii.plucinskijakub.laboratorium02;

import java.util.Random;
import java.util.Scanner;

public class Zadanie2g {
    public static void main(String[] args)
    {
        Scanner in = new Scanner(System.in);

        System.out.print("Podaj ilość liczb: ");
        int n = in.nextInt();
        int[] tab;
        tab = new int[n];
        generuj(tab, n, 999, 1998);
        System.out.println("");
        System.out.print("Podaj lewą liczbę mniejszą niż n: ");
        int lewa = in.nextInt();
        System.out.print("Podaj prawą liczbę, mniejszą niż lewa i mniejszą niż n: ");
        int prawa = in.nextInt();
        odwrocFragment(tab, lewa, prawa);
    }
    public static void generuj(int tab[], int n, int minWartosc, int maxWartosc)
    {
        Random r = new Random();
        for(int j = 0; j < n; j++)
        {
            tab[j] = r.nextInt(maxWartosc) - minWartosc;
            System.out.print(tab[j] + " ");
        }
    }
    public static void odwrocFragment(int[] tab, int lewy, int prawy)
    {
        for(int i = 0; i < tab.length; i++)
        {
            int[] tap;
            tap = new int[tab.length];
            tap = tab;
            if(i==lewy)
            {
                int odlicz =0, tmp;
                for(int u = lewy; u < prawy-1; u++)
                {
                    tmp = tap[u];
                    tap[u] = tap[prawy - odlicz];
                    tap[prawy-odlicz] = tmp;
                    odlicz++;
                }
            }
            System.out.print(tap[i] + " ");
        }
    }
}
