package pl.uwm.wmii.plucinskijakub.laboratorium02;

import java.util.Random;
import java.util.Scanner;

public class Zadanie2c {
    public static void main(String[] args)
    {
        Scanner in = new Scanner(System.in);

        System.out.print("Podaj ilość liczb: ");
        int n = in.nextInt();
        int[] tab;
        tab = new int[n];
        generuj(tab, n, 999, 1998);
        int ileM = ileMaksymalnych(tab);
        System.out.println("");
        System.out.println("Ilość liczb o wartosci maksymalnej : " + ileM);
    }
    public static void generuj(int tab[], int n, int minWartosc, int maxWartosc)
    {
        Random r = new Random();
        for(int j = 0; j < n; j++)
        {
            tab[j] = r.nextInt(maxWartosc) - minWartosc;
            System.out.print(tab[j] + " ");
        }
    }
    public static int ileMaksymalnych(int[] tab)
    {
        int ile = 0;
        int najwieksza = tab[0];
        for(int i = 1; i<tab.length;i++)
        {
            if(tab[i] > najwieksza) najwieksza = tab[i];
        }
        for(int i = 0; i < tab.length; i++)
        {
            if( tab[i] == najwieksza) ile++;
        }
        System.out.println("");
        System.out.println("Najwieksza liczba to: " + najwieksza);
        return ile;
    }
}
