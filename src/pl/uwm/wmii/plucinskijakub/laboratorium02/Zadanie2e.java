package pl.uwm.wmii.plucinskijakub.laboratorium02;

import java.util.Random;
import java.util.Scanner;

public class Zadanie2e {
    public static void main(String[] args)
    {
        Scanner in = new Scanner(System.in);

        System.out.print("Podaj ilość liczb: ");
        int n = in.nextInt();
        int[] tab;
        tab = new int[n];
        generuj(tab, n, 999, 1998);
        int maks = dlugoscMaksymalnegoCiaguDodatnich(tab);
        System.out.println("");
        System.out.println("Dlugosc maksymalnego ciagu liczb dodatnich : " + maks);
    }
    public static void generuj(int tab[], int n, int minWartosc, int maxWartosc)
    {
        Random r = new Random();
        for(int j = 0; j < n; j++)
        {
            tab[j] = r.nextInt(maxWartosc) - minWartosc;
            System.out.print(tab[j] + " ");
        }
    }
    public static int dlugoscMaksymalnegoCiaguDodatnich(int[] tab)
    {
        int[] tap;
        tap = new int[tab.length];
        int tmp =0;
        for(int i = 0; i <tab.length; i++)
        {
            if(tab[i] > 0)
            {
                tmp++;
                tap[i] = tmp;
            }
            if(tab[i] < 0)
            {
                tap[i] = tmp;
                tmp = 0;
            }
        }
        int dlugoscfrag = tap[0];
        for(int i = 1; i < tab.length; i++)
        {
            if(tap[i] > dlugoscfrag) dlugoscfrag = tap[i];
        }
        return dlugoscfrag;
    }
}
