package pl.uwm.wmii.plucinskijakub.laboratorium02;

import java.util.Random;
import java.util.Scanner;
import java.util.Arrays;

public class Zadanie3 {
    public static void main(String[] args)
    {
        Scanner in = new Scanner(System.in);
        System.out.print("Podaj 3 cyfry: ");
        int m = in.nextInt();
        int n = in.nextInt();
        int k = in.nextInt();
        System.out.println();
        int macierzA[][] = new int[m][n];
        losowaMacierz(macierzA, m, n);
        System.out.println();
        int macierzB[][] = new int[n][k];
        losowaMacierz(macierzB, n, k);
        System.out.println();
    }
    public static void losowaMacierz(int macierz[][], int m, int n)
    {
        Random r = new Random();
        macierz = new int[m][n];
        for(int i = 0; i <=n; i++)
        {
            for(int j = 0; j <=m; j++)
            {
                macierz[i][j] = r.nextInt(10);
                System.out.print(macierz[i][j] + " ");
                if(j==m) System.out.println();
            }
        }
    }
}
