package pl.uwm.wmii.plucinskijakub.laboratorium02;

import java.util.Random;
import java.util.Scanner;

public class Zadanie2d {
    public static void main(String[] args)
    {
        Scanner in = new Scanner(System.in);

        System.out.print("Podaj ilość liczb: ");
        int n = in.nextInt();
        int[] tab;
        tab = new int[n];
        generuj(tab, n, 999, 1998);
        int sumaU = sumaUjemnych(tab);
        int sumaD = sumaDodatnich(tab);
        System.out.println("");
        System.out.println("Suma liczb ujemnych : " + sumaU);
        System.out.println("Suma liczb dodatnich : " + sumaD);
    }
    public static void generuj(int tab[], int n, int minWartosc, int maxWartosc)
    {
        Random r = new Random();
        for(int j = 0; j < n; j++)
        {
            tab[j] = r.nextInt(maxWartosc) - minWartosc;
            System.out.print(tab[j] + " ");
        }
    }
    public static int sumaDodatnich(int[] tab)
    {
        int sumadod = 0;
        for(int i = 0; i <tab.length; i++)
        {
            if(tab[i] > 0) sumadod+=tab[i];
        }
        return sumadod;
    }
    public static int sumaUjemnych(int[] tab)
    {
        int sumauje = 0;
        for(int i = 0; i <tab.length; i++)
        {
            if(tab[i] < 0) sumauje+=tab[i];
        }
        return sumauje;
    }
}
