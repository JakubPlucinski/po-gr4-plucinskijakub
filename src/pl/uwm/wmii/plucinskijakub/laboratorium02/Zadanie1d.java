package pl.uwm.wmii.plucinskijakub.laboratorium02;

import java.util.Random;
import java.util.Scanner;

public class Zadanie1d {
    public static void main(String[] args)
    {
        Scanner in = new Scanner(System.in);

        System.out.print("Podaj ilość liczb: ");
        int n = in.nextInt();
        int[] tab;
        tab = new int[n];
        int sumadod=0, sumauje=0;
        Random r =new Random();
        for(int i = 0; i <n; i++)
        {
            tab[i] = r.nextInt(1998) -999;
            System.out.print(tab[i] + " ");
        }
        for(int i = 0; i <n; i++)
        {
            if(tab[i] < 0) sumauje+=tab[i];
            if(tab[i] > 0) sumadod+=tab[i];
        }

        System.out.println("");
        System.out.println("Najwieksza liczba to: " + sumauje);
        System.out.println("Ilosc liczb nawiekszych: " + sumadod);
    }
}
