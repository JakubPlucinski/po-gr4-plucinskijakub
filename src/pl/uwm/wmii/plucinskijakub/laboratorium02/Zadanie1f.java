package pl.uwm.wmii.plucinskijakub.laboratorium02;

import java.util.Random;
import java.util.Scanner;

public class Zadanie1f {
    public static void main(String[] args)
    {
        Scanner in = new Scanner(System.in);

        System.out.print("Podaj ilość liczb: ");
        int n = in.nextInt();
        int[] tab;
        tab = new int[n];
        Random r =new Random();
        for(int i = 0; i <n; i++)
        {
            tab[i] = r.nextInt(1998) -999;
            System.out.print(tab[i] + " ");
        }
        System.out.println("");
        for(int i = 0; i <n; i++)
        {
            if(tab[i] < 0) tab[i] = -1;
            if(tab[i] > 0) tab[i] = 1;
            System.out.print(tab[i] + " ");
        }
    }
}
