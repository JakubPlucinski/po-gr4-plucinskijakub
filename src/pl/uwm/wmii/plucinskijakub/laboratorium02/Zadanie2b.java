package pl.uwm.wmii.plucinskijakub.laboratorium02;

import java.util.Random;
import java.util.Scanner;

public class Zadanie2b {
    public static void main(String[] args)
    {
        Scanner in = new Scanner(System.in);

        System.out.print("Podaj ilość liczb: ");
        int n = in.nextInt();
        int[] tab;
        tab = new int[n];
        generuj(tab, n, 999, 1998);
        int ileD = ileDodatnich(tab);
        int ileU = ileUjemnych(tab);
        int ileZ = ileZerowych(tab);
        System.out.println("");
        System.out.println("Ilość liczb dodatnich: " + ileD);
        System.out.println("Ilość liczb ujemnych: " + ileU);
        System.out.println("Ilość zer: " + ileZ);
    }
    public static void generuj(int tab[], int n, int minWartosc, int maxWartosc)
    {
        Random r = new Random();
        for(int j = 0; j < n; j++)
        {
            tab[j] = r.nextInt(maxWartosc) - minWartosc;
            System.out.print(tab[j] + " ");
        }
    }
    public static int ileDodatnich(int[] tab)
    {
        int dodatnie = 0;
        for(int i = 0; i <tab.length; i++)
        {
            if(tab[i]>0) dodatnie++;
        }
        return dodatnie;
    }
    public static int ileUjemnych(int[] tab)
    {
        int ujemne = 0;
        for(int i = 0; i <tab.length; i++)
        {
            if(tab[i]<0) ujemne++;
        }
        return ujemne;
    }
    public static int ileZerowych(int[] tab)
    {
        int zera = 0;
        for(int i = 0; i <tab.length; i++)
        {
            if(tab[i]==0) zera++;
        }
        return zera;
    }
}
