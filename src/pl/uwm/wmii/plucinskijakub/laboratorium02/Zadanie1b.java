package pl.uwm.wmii.plucinskijakub.laboratorium02;

import java.util.Random;
import java.util.Scanner;

public class Zadanie1b {
    public static void main(String[] args)
    {
        Scanner in = new Scanner(System.in);

        System.out.print("Podaj ilość liczb: ");
        int n = in.nextInt();
        int[] tab;
        tab = new int[n];
        int ujemne=0, dodatnie =0, zerowe=0;
        Random r =new Random();
        for(int i = 0; i <n; i++)
        {
            tab[i] = r.nextInt(1998) -999;
            System.out.print(tab[i] + " ");
            if(tab[i] <0) ujemne++;
            if(tab[i] >0) dodatnie++;
            if(tab[i] == 0) zerowe++;
        }
        System.out.println("");
        System.out.println("Ilość liczb ujemnych: " + ujemne);
        System.out.println("Ilość liczb dodatnich: " + dodatnie);
        System.out.println("Ilość zer: " + zerowe);
    }
}
