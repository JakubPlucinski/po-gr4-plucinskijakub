package pl.uwm.wmii.plucinskijakub.laboratorium03_2;

import java.util.ArrayList;
import java.util.Arrays;

public class Zadanie5 {
    public static void main(String[] args)
    {
        ArrayList<Integer> a = new ArrayList<Integer>();
        a.add(5);
        a.add(3);
        a.add(6);
        reverse(a);
        System.out.printf(Arrays.toString(a.toArray()));
    }
    public static void reverse(ArrayList<Integer> a)
    {
        ArrayList<Integer> wynik = new ArrayList<Integer>();
        for(int i = a.size() - 1; i >=0; i--) wynik.add(a.get(i));
        a.clear();
        for(int i = 0; i < wynik.size(); i++) a.add(wynik.get(i));
    }
}
