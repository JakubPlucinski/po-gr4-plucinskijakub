package pl.uwm.wmii.plucinskijakub.laboratorium03_2;

import java.util.ArrayList;
import java.util.Arrays;

public class Zadanie4 {
    public static void main(String[] args)
    {
        ArrayList<Integer> a = new ArrayList<Integer>();
        a.add(5);
        a.add(3);
        a.add(6);
        System.out.printf(Arrays.toString(reversed(a).toArray()));
    }
    public static ArrayList<Integer> reversed(ArrayList<Integer> a)
    {
        ArrayList<Integer> wynik = new ArrayList<Integer>();
        for(int i = a.size() - 1; i >=0; i--) wynik.add(a.get(i));
        return wynik;
    }
}
