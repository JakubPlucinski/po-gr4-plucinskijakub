package pl.uwm.wmii.plucinskijakub.laboratorium03_2;

import java.util.ArrayList;
import java.util.Arrays;

public class Zadanie3 {
    public static void main(String[] args)
    {
        ArrayList<Integer> a = new ArrayList<Integer>();
        ArrayList<Integer> b = new ArrayList<Integer>();
        a.add(5);
        a.add(3);
        a.add(6);
        a.add(7);
        a.add(0);
        b.add(3);
        b.add(11);
        b.add(22);
        System.out.printf(Arrays.toString(mergeSorted(a, b).toArray()));
    }
    public static ArrayList<Integer> mergeSorted(ArrayList<Integer> a, ArrayList<Integer> b)
    {
        ArrayList<Integer> wynik = new ArrayList<Integer>();
        int i, j;
        for(i = 0, j = 0; i < a.size(); i++)
        {
            for(int k = j; k < b.size(); k++)
            {
                if(a.get(i) >= b.get(i))
                {
                    wynik.add(b.get(k));
                    j++;
                }
            }
            wynik.add(a.get(i));
        }
        for(; j < b.size(); j++) wynik.add(b.get(j));
        return wynik;
    }
}
