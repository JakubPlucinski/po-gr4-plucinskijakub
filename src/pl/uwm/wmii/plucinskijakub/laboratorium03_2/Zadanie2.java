package pl.uwm.wmii.plucinskijakub.laboratorium03_2;

import java.util.ArrayList;
import java.util.Arrays;

public class Zadanie2 {
    public static void main(String[] args)
    {
        ArrayList<Integer> a = new ArrayList<Integer>();
        ArrayList<Integer> b = new ArrayList<Integer>();
        a.add(5);
        a.add(3);
        a.add(6);
        a.add(7);
        a.add(0);
        b.add(3);
        b.add(11);
        b.add(22);
        System.out.printf(Arrays.toString(merge(a, b).toArray()));
    }
    public static ArrayList<Integer> merge(ArrayList<Integer> a, ArrayList<Integer> b)
    {
        ArrayList<Integer> wynik = new ArrayList<Integer>();
        boolean a_wieksza = a.size() > b.size() ? true : false;
        int wieksza = a.size() > b.size() ? a.size() : b.size();
        int mniejsza = wieksza == a.size() ? b.size() : a.size();
        int i;
        for(i = 0; i < mniejsza; i++)
        {
            wynik.add(a.get(i));
            wynik.add(b.get(i));
        }
        for(; i < wieksza; i++)
        {
            if(a_wieksza)
            {
                wynik.add(a.get(i));
            }
            else
            {
                wynik.add(b.get(i));
            }
        }
        return wynik;
    }
}
