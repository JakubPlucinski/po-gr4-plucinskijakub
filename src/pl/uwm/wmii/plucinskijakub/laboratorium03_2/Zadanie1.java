package pl.uwm.wmii.plucinskijakub.laboratorium03_2;

import java.util.ArrayList;
import java.util.Arrays;;

public class Zadanie1 {
    public static void main(String[] args)
    {
        ArrayList<Integer> a = new ArrayList<Integer>();
        ArrayList<Integer> b = new ArrayList<Integer>();
        a.add(5);
        a.add(3);
        a.add(6);
        b.add(3);
        b.add(9);
        b.add(2);
        System.out.printf(Arrays.toString(append(a, b).toArray()));
    }
    public static ArrayList<Integer> append(ArrayList<Integer> a, ArrayList<Integer> b)
    {
        ArrayList<Integer> wynik = new ArrayList<Integer>();
        for(int i = 0; i < a.size(); i++) wynik.add(a.get(i));
        for(int i = 0; i < b.size(); i++) wynik.add(b.get(i));
        return wynik;
    }
}
