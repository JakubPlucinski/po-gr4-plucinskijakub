package pl.uwm.wmii.plucinskijakub.laboratorium04;

public class Zadanie1 {
    public static void main(String[] args){
        RachunekBankowy saver1 = new RachunekBankowy(2000);
        RachunekBankowy saver2 = new RachunekBankowy(3000);
        RachunekBankowy.ustawRocznaStopeProcentowa(0.04);
        System.out.println("Odestki po pierwszym miesiacu");
        System.out.println(String.format("Klient nr 1: %.2f\nKlient nr 2: %.2f", saver1.obliczMiesieczneOdsetki(), saver2.obliczMiesieczneOdsetki()));
        saver1.dodaj(saver1.obliczMiesieczneOdsetki());
        saver2.dodaj(saver2.obliczMiesieczneOdsetki());
        RachunekBankowy.ustawRocznaStopeProcentowa(0.05);
        System.out.println("Odestki po drugim miesiacu");
        System.out.println(String.format("Klient nr 1: %.2f\nKlient nr 2: %.2f", saver1.obliczMiesieczneOdsetki(), saver2.obliczMiesieczneOdsetki()));
        saver1.dodaj(saver1.obliczMiesieczneOdsetki());
        saver2.dodaj(saver2.obliczMiesieczneOdsetki());
    }


}
