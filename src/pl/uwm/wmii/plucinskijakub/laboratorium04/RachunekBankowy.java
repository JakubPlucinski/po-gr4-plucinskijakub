package pl.uwm.wmii.plucinskijakub.laboratorium04;

public class RachunekBankowy {
    public RachunekBankowy(double saldoKlienta)
    {
        saldo = saldoKlienta;
    }
    public double obliczMiesieczneOdsetki()
    {
        return (saldo*rocznaStopaProcentowa)/12;
    }
    public void dodaj(double ile)
    {
        saldo += obliczMiesieczneOdsetki();
    }
    public static void ustawRocznaStopeProcentowa(double wartosc)
    {
        rocznaStopaProcentowa = wartosc;
    }
    private double saldo;
    public static double rocznaStopaProcentowa;
}
