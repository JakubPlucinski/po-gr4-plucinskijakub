package pl.uwm.wmii.plucinskijakub.laboratorium06;

import pl.imiajd.plucinski.OsobaDwa;
import pl.imiajd.plucinski.PracownikDwa;
import pl.imiajd.plucinski.StudentDwa;

import java.time.LocalDate;

public class Zadanie1
{
    public static void main(String[] args)
    {
        OsobaDwa[] ludzie = new OsobaDwa[2];
        String[] imiona1 = new String[2];
        imiona1[0] = "Jan";
        imiona1[1] = "Adam";
        String[] imiona2 = new String[2];
        imiona2[0] = "Małgorzata";
        imiona2[1] = "Barbara";

        ludzie[0] = new PracownikDwa(imiona1,"Kowalski", LocalDate.of(1970, 3, 12),true, LocalDate.of(2018, 9, 10), 50000);
        ludzie[1] = new StudentDwa(imiona2, "Nowak", LocalDate.of(1995, 5, 21), false, 3.22, "informatyka");
        // ludzie[2] = new Osoba("Dyl Sowizdrzał");

        for (OsobaDwa p : ludzie) {
            System.out.println(p.getOpis());
        }
    }
}
