package pl.uwm.wmii.plucinskijakub.laboratorium03;

import java.math.BigDecimal;

public class Zadanie5 {
    public static BigDecimal kapital(BigDecimal k, int p, int n) {
        BigDecimal wynik = k;
        for (int i = 0; i < n; i++)
            wynik = wynik.multiply(BigDecimal.ONE.add(BigDecimal.valueOf(p).divide(BigDecimal.valueOf(100))));
        return wynik;
    }
    public static void main(String[] args) {
        System.out.println(kapital(BigDecimal.valueOf(10000), 4, 3));
    }
}
