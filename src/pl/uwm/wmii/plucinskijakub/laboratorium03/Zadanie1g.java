package pl.uwm.wmii.plucinskijakub.laboratorium03;

public class Zadanie1g {
    public static void main(String[] args)
    {
        String a = "123456789";
        System.out.print(nice(a));
    }
    public static String nice(String str) {
        StringBuffer reversed = new StringBuffer(str);
        reversed.reverse();
        StringBuffer result = new StringBuffer();
        for (int i = 1, j = 0; j < reversed.length(); i++) {
            if (i % 4 == 0)
                result.append('\'');
            else {
                result.append(reversed.charAt(j));
                j++;
            }
        }
        result.reverse();
        return result.toString();
    }
}
