package pl.uwm.wmii.plucinskijakub.laboratorium03;

public class Zadanie1b {
    public static void main(String[] args)
    {
        String a = "abaabadef";
        String b = "aba";
        System.out.print("Ilosc wystapien: " + countSubStr(a, b));

    }
    public static int countSubStr(String str, String subStr)
    {
        int wystapienie = 0;
        for(int i = 0; i < str.length(); i++)
        {
            if(str.indexOf(subStr) != -1) wystapienie++;
            str = str.replaceFirst(subStr, "!");
        }
        return  wystapienie;
    }
}
