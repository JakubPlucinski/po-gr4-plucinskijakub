package pl.uwm.wmii.plucinskijakub.laboratorium03;

public class Zadanie1f {
    public static void main(String[] args)
    {
        String a = "abaDDbadef";
        System.out.print(change(a));

    }
    public static String change(String str) {
        StringBuffer result = new StringBuffer();
        for (char c : str.toCharArray())
            result.append((int)c >= 65 && (int)c <= 90 ? (char)((int)c + 32) : (char)((int)c - 32));
        return result.toString();
    }
}
