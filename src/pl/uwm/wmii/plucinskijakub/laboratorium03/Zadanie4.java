package pl.uwm.wmii.plucinskijakub.laboratorium03;

import java.math.BigInteger;

public class Zadanie4 {
    public static BigInteger szachownica(int n) {
        BigInteger wynik = BigInteger.ZERO;
        for (int i = 0; i < n; i++) {
            wynik = wynik.add(BigInteger.valueOf(2).pow(i));
        }
        return wynik;
    }
    public static void main(String[] args) {
        int n = 12;
        System.out.println(szachownica(n));
    }
}
