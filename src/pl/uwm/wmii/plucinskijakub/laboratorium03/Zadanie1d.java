package pl.uwm.wmii.plucinskijakub.laboratorium03;

import java.util.Scanner;

public class Zadanie1d {
    public static void main(String[] args)
    {
        Scanner in = new Scanner(System.in);
        System.out.println("Wpisz napis:");
        String a = in.next();
        System.out.println("Wpisz ile razy powtorzyc:");
        int n = in.nextInt();
        System.out.print("Ciag znakow po powtorzeniu n razy: " + repeat(a, n));
    }
    public static String repeat(String str, int n)
    {
        String kon = str;
        for(int i = 0; i < n; i++)
        {
            kon += str;
        }
        return kon;
    }
}
