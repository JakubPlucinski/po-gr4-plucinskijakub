package pl.uwm.wmii.plucinskijakub.laboratorium03;

import java.io.File;
import java.util.Scanner;

public class Zadanie3 {
    public static void main(String[] args) throws Exception {
        System.out.println(countSubStrInFile("D:\\UWM\\Semestr 3\\Programowanie Obiektowe\\po-gr4-plucinskijakub\\src\\pl\\uwm\\wmii\\plucinskijakub\\laboratorium03\\input.txt", "test"));
    }
    public static int countSubStr(String str, String subStr) {
        if (str.indexOf(subStr) == -1)
            return 0;
        return 1 + countSubStr(str.replaceFirst(subStr, ""), subStr);
    }
    public static String readFile(String fileName) throws Exception {
        File file = new File(fileName);
        Scanner sc = new Scanner(file);
        StringBuffer sb = new StringBuffer();
        while (sc.hasNextLine())
            sb.append(sc.nextLine());
        return sb.toString();
    }
    public static int countSubStrInFile(String fileName, String subStr) throws Exception {
        return countSubStr(readFile(fileName), subStr);
    }
}
