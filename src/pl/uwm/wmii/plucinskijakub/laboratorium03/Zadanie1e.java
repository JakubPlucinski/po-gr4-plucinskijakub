package pl.uwm.wmii.plucinskijakub.laboratorium03;

import java.util.Scanner;
import java.util.Arrays;
import static pl.uwm.wmii.plucinskijakub.laboratorium03.Zadanie1b.countSubStr;

public class Zadanie1e {
    public static void main(String[] args)
    {
        Scanner in = new Scanner(System.in);
        String a = "addeadad";
        String b = "ad";
        System.out.println(Arrays.toString(where(a, b)));
    }
    public static int[] where(String str, String subStr) {
        int[] wynik = new int[countSubStr(str, subStr)];
        for (int i = 0, j = 0; i >= 0;) {
            i = str.indexOf(subStr, i + 1);
            if (i != -1)
                wynik[j++] = i;
        }
        return wynik;
    }
}
