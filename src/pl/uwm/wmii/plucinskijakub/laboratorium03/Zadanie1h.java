package pl.uwm.wmii.plucinskijakub.laboratorium03;

public class Zadanie1h {
    public static void main(String[] args)
    {
        String a = "123456789";
        System.out.print(nice(a, 't', 2));
    }
    public static String nice(String str, char sep, int pos) {
        StringBuffer reversed = new StringBuffer(str);
        reversed.reverse();
        StringBuffer result = new StringBuffer();
        for (int i = 1, j = 0; j < reversed.length(); i++) {
            if (i % (pos + 1) == 0)
                result.append(sep);
            else {
                result.append(reversed.charAt(j));
                j++;
            }
        }
        result.reverse();
        return result.toString();
    }
}
