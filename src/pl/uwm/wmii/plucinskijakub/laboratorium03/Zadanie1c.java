package pl.uwm.wmii.plucinskijakub.laboratorium03;

import java.util.Scanner;

public class Zadanie1c {
    public static void main(String[] args)
    {
        Scanner in = new Scanner(System.in);
        System.out.println("Wpisz napis:");
        String a = in.next();
        System.out.print("Srodkowy znak/znaki to: " + middle(a));
    }
    public static String middle(String str)
    {
        if(str.length()%2 ==0) return Character.toString(str.charAt(str.length()/2-1))+Character.toString(str.charAt(str.length()/2));
        else return Character.toString(str.charAt(str.length()/2));
    }
}
