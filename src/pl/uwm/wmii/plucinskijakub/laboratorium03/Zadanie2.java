package pl.uwm.wmii.plucinskijakub.laboratorium03;

import java.io.File;
import java.util.Scanner;

public class Zadanie2 {
    public static void main(String[] args) throws Exception {
        System.out.println(countCharInFile("D:\\UWM\\Semestr 3\\Programowanie Obiektowe\\po-gr4-plucinskijakub\\src\\pl\\uwm\\wmii\\plucinskijakub\\laboratorium03\\input.txt", 't'));
    }
    public static int countChar(String str, char c) {
        if (str.indexOf(Character.toString(c)) == -1)
            return 0;
        return 1 + countChar(str.replaceFirst(Character.toString(c), ""), c);
    }
    public static String readFile(String fileName) throws Exception {
        File file = new File(fileName);
        Scanner sc = new Scanner(file);
        StringBuffer sb = new StringBuffer();
        while (sc.hasNextLine())
            sb.append(sc.nextLine());
        return sb.toString();
    }
    public static int countCharInFile(String fileName, char c) throws Exception {
        return countChar(readFile(fileName), c);
    }
}
